package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type factorialResponse struct {
	Input  uint64
	Output uint64
}

// our main function
func main() {
	router := mux.NewRouter()
	router.HandleFunc("/factorial/{number:[0-9]+}", GetFactorial).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}

// GetFactorial Return factorial of input parameter
func GetFactorial(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	number, _ := strconv.ParseUint(params["number"], 10, 64)

	response := factorialResponse{
		Input:  number,
		Output: factorial(number),
	}

	json.NewEncoder(w).Encode(response)
}

func factorial(n uint64) (result uint64) {
	if n > 0 {
		result = n * factorial(n-1)
		return result
	}
	return 1
}
