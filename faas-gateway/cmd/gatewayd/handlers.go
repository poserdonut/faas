package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/gorilla/mux"
)

// Proxy is it me you're looking for
func Proxy(w http.ResponseWriter, r *http.Request) {
	functionName := mux.Vars(r)["function"]

	if val, ok := faasContainers[functionName]; ok {
		w.Header().Set("Content-Type", "application/json")
		faasFunctionURL := fmt.Sprintf("http://%s:%d%s%s", val.Name, val.Port, r.URL.Path, getQueryParams(r.URL.RawQuery))
		resp, err := http.Get(faasFunctionURL)

		fmt.Printf("%v \n", faasFunctionURL)

		if err != nil {
			panic(err)
		}

		io.Copy(w, resp.Body)
		resp.Body.Close()

	} else {
		fmt.Fprintf(w, "No faas function with the name: %s", functionName)
	}
}

func getQueryParams(query string) string {
	if len(query) > 0 {
		return "?" + query
	}

	return ""
}

// Containers List containers
func Containers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(faasContainers)
}
