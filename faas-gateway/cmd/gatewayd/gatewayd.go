package main

import (
	"log"
	"net/http"
	"time"
)

func main() {
	router := NewRouter()
	GetContainers()
	go Poll()
	log.Fatal(http.ListenAndServe(":8989", router))
}

// Poll Poll container changes
func Poll() {
	c := time.Tick(10 * time.Second)
	for range c {
		GetContainers()
	}
}
