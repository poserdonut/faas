package main

import (
	"context"
	"fmt"
	"strconv"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

// NameLabel the name of the label which defines faas name
const NameLabel = "faas.name"

// PortLabel the name of the label which defines faas port
const PortLabel = "faas.port"

// FaasContainer contains name and port of faas service
type FaasContainer struct {
	Name string
	Port int
}

var faasContainers map[string]FaasContainer

func init() {
	faasContainers = make(map[string]FaasContainer)
}

// GetContainers Use Docker client and get containers
func GetContainers() {
	cli, err := client.NewClientWithOpts(client.WithVersion("1.37"))

	if err != nil {
		panic(err)
	}

	listCointainers(cli)
}

func listCointainers(cli *client.Client) {
	filters := filters.NewArgs()
	filters.Add("label", NameLabel)

	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{
		Filters: filters,
	})
	if err != nil {
		panic(err)
	}

	updatedContainerMap := make(map[string]FaasContainer)

	for _, container := range containers {
		name := container.Labels[NameLabel]
		port, _ := strconv.Atoi(container.Labels[PortLabel])

		faasContainer := FaasContainer{Name: name, Port: port}

		updatedContainerMap[name] = faasContainer
	}

	faasContainers = updatedContainerMap

	fmt.Printf("%v \n", faasContainers)
}
