package main

import (
	"github.com/gorilla/mux"
)

// NewRouter Setup router
func NewRouter() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/containers", Containers).Methods("GET")
	router.HandleFunc("/{function}/{rest:.*}", Proxy).Methods("GET")
	router.HandleFunc("/{function}", Proxy).Methods("GET")

	return router
}
