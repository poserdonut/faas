package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sparrc/go-ping"
)

// our main function
func main() {
	router := mux.NewRouter()
	router.HandleFunc("/ping/{url}", Ping).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}

// Ping Return factorial of input parameter
func Ping(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	url := params["url"]

	pinger, err := ping.NewPinger(url)

	if err != nil {
		fmt.Fprintf(w, "Error: %s", err.Error())
		return
	}

	pinger.OnFinish = func(stats *ping.Statistics) {
		fmt.Fprintf(w, "%d packets transmitted, %d packets received, %v%% packet loss", stats.PacketsSent, stats.PacketsRecv, stats.PacketLoss)
	}

	pinger.Count = 1
	pinger.Timeout = 5 * 1000000000
	pinger.Run()
}
