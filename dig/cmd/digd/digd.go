package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/lixiangzhong/dnsutil"
)

type factorialResponse struct {
	Input  uint64
	Output uint64
}

// our main function
func main() {
	router := mux.NewRouter()
	router.StrictSlash(true)
	router.HandleFunc("/dig", DigDug).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}

// DigDug Return dig response of input parameter
func DigDug(w http.ResponseWriter, r *http.Request) {

	queryParams := r.URL.Query()
	hostname := queryParams.Get("hostname")

	var dig dnsutil.Dig
	dig.SetDNS("8.8.8.8")
	a, err := dig.A(hostname)

	if err != nil {
		panic(err)
	}

	json.NewEncoder(w).Encode(a)
}
